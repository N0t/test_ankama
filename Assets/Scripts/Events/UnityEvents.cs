﻿namespace Test.Events {
    using Test.Data;
    using UnityEngine.Events;
    using System.Collections.Generic;
    using System;

    [Serializable] public class IntEvent : UnityEvent<int> { }
    [Serializable] public class StringEvent : UnityEvent<string> { }
    [Serializable] public class StringsEvent : UnityEvent<IEnumerable<string>> { }
    [Serializable] public class CharacterClassEvent : UnityEvent<CharacterClass> { }
    [Serializable] public class CharacterEvent : UnityEvent<Character> { }
    [Serializable] public class SpellEvent : UnityEvent<Spell> { }
    
}