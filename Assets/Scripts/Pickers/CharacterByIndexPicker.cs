﻿namespace Test.Pickers {
    using Test.Data;
    using Test.Events;
    using UnityEngine;

    // Picks a character from the Game Data List, at the given index
    public class CharacterByIndexPicker : MonoBehaviour {

        [SerializeField] int indexToPick;
        [SerializeField] CharacterEvent pickedCharacter;

        public void Pick() => pickedCharacter.Invoke(GameDataList.Characters[indexToPick]);
        public void Pick(int index) {
            indexToPick = index;
            Pick();
        }

    }

}