﻿namespace Test.Injectors {
    using Test.Data;
    using Test.Events;
    using UnityEngine;

    public class SpellInjector : MonoBehaviour {
        [SerializeField] StringEvent inGameName;
        [SerializeField] StringEvent paCost;
        [SerializeField] StringEvent damage;

        public void Inject(Spell spell) {
            inGameName.Invoke(spell?.InGameName);
            paCost.Invoke(spell?.PACost.ToString());
            damage.Invoke(spell?.Damage.ToString());
        }

    }

}
