﻿namespace Test.Injectors {
    using Test.Data;
    using Test.Events;
    using UnityEngine;
    using System.Collections.Generic;

    public class GameDataListInjector : MonoBehaviour {
        [SerializeField] StringsEvent spellNames;
        [SerializeField] StringsEvent characterNames;

        public void Inject() {
            List<string> injSpellNames = new List<string>();
            GameDataList.Spells.ForEach(spell => injSpellNames.Add(spell.InGameName));
            spellNames.Invoke(injSpellNames);

            List<string> injCharacterNames = new List<string>();
            GameDataList.Characters.ForEach(character => injCharacterNames.Add(character.InGameName));
            characterNames.Invoke(injCharacterNames);
        }

    }

}