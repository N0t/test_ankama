﻿namespace Test.Injectors {
    using Test.Data;
    using Test.Events;
    using UnityEngine;

    public class CharacterInjector : MonoBehaviour {
        [SerializeField] StringEvent inGameName;
        [SerializeField] StringEvent characterclass;
        [SerializeField] StringEvent hp;
        [SerializeField] StringEvent pa;
        [SerializeField] SpellEvent spell1;
        [SerializeField] SpellEvent spell2;
        [SerializeField] SpellEvent spell3;
        [SerializeField] SpellEvent spell4;

        public void Inject(Character character) {
            inGameName.Invoke(character.InGameName);
            characterclass.Invoke(character.CharacterClass.ToString());
            hp.Invoke(character.HP.ToString());
            pa.Invoke(character.PA.ToString());
            spell1.Invoke(character.Spells[0]);
            spell2.Invoke(character.Spells[1]);
            spell3.Invoke(character.Spells[2]);
            spell4.Invoke(character.Spells[3]);
        }

    }

}
