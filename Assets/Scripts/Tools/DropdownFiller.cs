﻿namespace Test.Tools {
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.Events;
    using static UnityEngine.UI.Dropdown;
    using System.Collections.Generic;

    // Fills a dropdown with a given string list
    public class DropdownFiller : MonoBehaviour {
        [SerializeField] Dropdown dropdown;

        [SerializeField] UnityEvent dropdownFilled;

        public void Fill(IEnumerable<string> content) {
            dropdown.ClearOptions();

            var options = new List<OptionData>();
            foreach (var obj in content) {
                options.Add(new OptionData(obj));
            }
            dropdown.AddOptions(options);

            dropdownFilled.Invoke();
        }
        
    }

}