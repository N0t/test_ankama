namespace Test.Tools {
    using Test.Data;

    using UnityEditor;

    using System.Collections.Generic;


    public static class DataResearch {

        public static List<T> GetAvailableGameData<T>(string nameFilter = "") where T : GameData {
            List<T> researchResults = new List<T>();

            string[] assetNames = AssetDatabase.FindAssets($"t:{typeof(T)} {nameFilter}", new[] { "Assets/Data" });
            researchResults.Clear();
            foreach (string resultName in assetNames) {
                var resultPath = AssetDatabase.GUIDToAssetPath(resultName);
                var result = AssetDatabase.LoadAssetAtPath<T>(resultPath);
                researchResults.Add(result);
            }

            return researchResults;
        }

    }

}
