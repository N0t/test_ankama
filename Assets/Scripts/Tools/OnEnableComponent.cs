﻿namespace Test.Tools {
    using UnityEngine;
    using UnityEngine.Events;

    public class OnEnableComponent : MonoBehaviour {
        [SerializeField] UnityEvent componentEnabled;

        void OnEnable() => componentEnabled.Invoke();
    }

}