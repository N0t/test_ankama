﻿namespace Test.Data {
    using Test.Tools;

    using UnityEngine;

    using System.Collections.Generic;

    [CreateAssetMenu(fileName = nameof(GameDataList), menuName = "Game Data/Game Data List")]
    public class GameDataList : ScriptableObject {

        static List<Spell> spells;
        static List<Character> characters;

        public static List<Spell> Spells {
            get {
                if (spells == default) { Refresh(); } // Initialize GameDataList if not already done
                return spells;
            }
        }

        public static List<Character> Characters {
            get {
                if (characters == default) { Refresh(); } // Initialize GameDataList if not already done
                return characters;
            }
        }

        void OnValidate() => Refresh();

        // GameDataList OnValidate() function gets called really late, using a public refresh button as a hotfix
        public static void Refresh() {
            spells = DataResearch.GetAvailableGameData<Spell>();
            characters = DataResearch.GetAvailableGameData<Character>();
        }
        
    }

}