﻿namespace Test.Data {
    using UnityEngine;

    [CreateAssetMenu(fileName = nameof(Spell), menuName = "Game Data/Spell")]
    public class Spell : GameData {
        
        [SerializeField] int paCost;
        [SerializeField] int damage;

        public int PACost { get => paCost; set => paCost = value; }
        public int Damage { get => damage; set => damage = value; }

    }

}
