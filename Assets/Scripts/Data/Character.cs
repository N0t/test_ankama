﻿namespace Test.Data {
    using UnityEngine;

    [CreateAssetMenu(fileName = nameof(Character), menuName = "Game Data/Character")]
    public class Character : GameData {

        [SerializeField] CharacterClass characterClass;
        [SerializeField] int hp;
        [SerializeField] int pa;
        [SerializeField] Spell[] spells = { null, null, null, null };

        public CharacterClass CharacterClass { get => characterClass; set => characterClass = value; }
        public int HP { get => hp; set => hp = value; }
        public int PA { get => pa; set => pa = value; }
        public Spell[] Spells => spells;
        
    }

}
