﻿namespace Test.Data {
    using UnityEngine;

    public abstract class GameData : ScriptableObject {
        [SerializeField] string inGameName;

        public string InGameName { get => inGameName; set => inGameName = value; }

    }

}
