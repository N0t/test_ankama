﻿namespace Test.Editor {
    using Test.Data;

    using UnityEditor;
    using UnityEngine;

    using System.Collections.Generic;

    public class GameDataInterface : EditorWindow {

        GameData selectedData; // Selected data that can be edited directly in the interface
        Editor gameDataEditor; // Integrated editor of the selected data

        // Data required for the display part
        string[] tabs = new string[] { "Spell", "Character" };
        int tabIndex = 0;
        int dropdownIndex = 0;

        // Data required for the forms
        Spell spellToCreate;
        Character characterToCreate;
        int characterSpellIndex1;
        int characterSpellIndex2;
        int characterSpellIndex3;
        int characterSpellIndex4;
        int characterClassIndex;
        List<CharacterClass> characterClass = new List<CharacterClass> { CharacterClass.Warrior, CharacterClass.Bowman, CharacterClass.Mage };


        void Awake() {
            spellToCreate = CreateInstance<Spell>();
            characterToCreate = CreateInstance<Character>();
        }

        [MenuItem("Window/Ankama <3/Game Data Interface")]
        static void Init() {
            GameDataInterface window = (GameDataInterface)GetWindow(typeof(GameDataInterface));
            window.Show();
        }

        private void OnGUI() {

            // Create different tabs for each data type
            tabIndex = GUILayout.Toolbar(tabIndex, tabs);


            // =========================================
            // === Data inspector selection dropdown ===
            // =========================================
            GUILayout.Space(10);
            GUILayout.Label($"{tabs[tabIndex]} Inspector");

            // Refresh Game Data List button, if an update was made out of the window
            GUI.backgroundColor = Color.blue;
            if (GUILayout.Button("Refresh Data List")) { GameDataList.Refresh(); }
            GUI.backgroundColor = Color.white;

            // Fill dropdown if at least one data is found (code depends on data type)
            switch (tabIndex) {
                case (0): // Spells dropdown

                    if (GameDataList.Spells.Count > 0) {
                        if (dropdownIndex >= GameDataList.Spells.Count) { dropdownIndex = 0; } // Avoid Out Of Range exception on tab switch
                        List<string> spellNames = new List<string>();
                        GameDataList.Spells.ForEach(spell => spellNames.Add(spell.name));
                        dropdownIndex = EditorGUILayout.Popup(dropdownIndex, spellNames.ToArray());
                        selectedData = GameDataList.Spells[dropdownIndex];
                    } else {
                        GUILayout.Label("No Data Found");
                    }
                    break;

                case (1): // === Characters dropdown ===

                    if (GameDataList.Characters.Count > 0) {
                        if (dropdownIndex >= GameDataList.Characters.Count) { dropdownIndex = 0; } // Avoid Out Of Range exception on tab switch
                        List<string> characterNames = new List<string>();
                        GameDataList.Characters.ForEach(character => characterNames.Add(character.name));
                        dropdownIndex = EditorGUILayout.Popup(dropdownIndex, characterNames.ToArray());
                        selectedData = GameDataList.Characters[dropdownIndex];
                    } else {
                        GUILayout.Label("No Data Found");
                    }
                    break;
            }

            // Selected data preparation for edition window
            if (selectedData != null) {
                gameDataEditor = Editor.CreateEditor(selectedData);
            } else {
                gameDataEditor = null;
            }

            gameDataEditor?.OnInspectorGUI();

            // Delete selected data
            GUI.backgroundColor = Color.red;
            if (GUILayout.Button($"Delete Selected {tabs[tabIndex]}")) {
                // Before anything, reset dropdown indexes that need to be, in order to avoid silly bugs
                characterSpellIndex1 = 0;
                characterSpellIndex2 = 0;
                characterSpellIndex3 = 0;
                characterSpellIndex4 = 0;

                AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(selectedData));
                GameDataList.Refresh();
            }
            GUI.backgroundColor = Color.white;


            // ======================================================
            // === Data creation form (code depends on data type) ===
            // ======================================================

            switch (tabIndex) {
                case (0): // Spells form
                    GUILayout.Space(30);
                    GUILayout.Label($"{tabs[tabIndex]} Creation");

                    GUILayout.Space(5);
                    GUILayout.Label("In Game Name :");
                    spellToCreate.InGameName = GUILayout.TextField(spellToCreate.InGameName);

                    GUILayout.Space(5);
                    GUILayout.Label("PA Cost :");
                    spellToCreate.PACost = EditorGUILayout.IntField(spellToCreate.PACost);

                    GUILayout.Space(5);
                    GUILayout.Label("Damage :");
                    spellToCreate.Damage = EditorGUILayout.IntField(spellToCreate.Damage);

                    GUILayout.Space(5);
                    if (GUILayout.Button($"Add A {tabs[tabIndex]}")) {
                        // Save data
                        AssetDatabase.CreateAsset(spellToCreate, $"Assets/Data/{tabs[tabIndex]}s/{spellToCreate.InGameName.ToLowerInvariant()}.asset");
                        AssetDatabase.SaveAssets();
                        AssetDatabase.Refresh();
                        GameDataList.Refresh();

                        spellToCreate = CreateInstance<Spell>(); // Prepare next creation
                    }
                    break;

                case (1): // Characters form
                    // Prepare spell and class name lists for dropdowns
                    List<string> spellNames = new List<string>();
                    GameDataList.Spells.ForEach(spell => spellNames.Add(spell.name));
                    List<string> classNames = new List<string>();
                    foreach (var charClass in characterClass) { classNames.Add(charClass.ToString()); }

                    GUILayout.Space(30);
                    GUILayout.Label($"{tabs[tabIndex]} Creation");

                    GUILayout.Space(5);
                    GUILayout.Label("In Game Name :");
                    characterToCreate.InGameName = GUILayout.TextField(characterToCreate.InGameName);

                    GUILayout.Space(5);
                    GUILayout.Label("HP :");
                    characterToCreate.HP = EditorGUILayout.IntField(characterToCreate.HP);

                    GUILayout.Space(5);
                    GUILayout.Label("PA :");
                    characterToCreate.PA = EditorGUILayout.IntField(characterToCreate.PA);

                    GUILayout.Space(5);
                    GUILayout.Label("Character Class :");
                    characterClassIndex = EditorGUILayout.Popup(characterClassIndex, classNames.ToArray());

                    GUILayout.Space(5);
                    if (GameDataList.Spells.Count > 0) {
                        GUILayout.Label("Spell 1 :");
                        characterSpellIndex1 = EditorGUILayout.Popup(characterSpellIndex1, spellNames.ToArray());

                        GUILayout.Space(5);
                        GUILayout.Label("Spell 2 :");
                        characterSpellIndex2 = EditorGUILayout.Popup(characterSpellIndex2, spellNames.ToArray());

                        GUILayout.Space(5);
                        GUILayout.Label("Spell 3 :");
                        characterSpellIndex3 = EditorGUILayout.Popup(characterSpellIndex3, spellNames.ToArray());

                        GUILayout.Space(5);
                        GUILayout.Label("Spell 4 :");
                        characterSpellIndex4 = EditorGUILayout.Popup(characterSpellIndex4, spellNames.ToArray());
                    } else {
                        GUILayout.Label("No Spells Data Found");
                    }

                    GUILayout.Space(5);
                    if (GUILayout.Button($"Add A {tabs[tabIndex]}")) {
                        // Prepare data
                        characterToCreate.CharacterClass = characterClass[characterClassIndex];
                        characterToCreate.Spells[0] = GameDataList.Spells[characterSpellIndex1];
                        characterToCreate.Spells[1] = GameDataList.Spells[characterSpellIndex2];
                        characterToCreate.Spells[2] = GameDataList.Spells[characterSpellIndex3];
                        characterToCreate.Spells[3] = GameDataList.Spells[characterSpellIndex4];

                        // Save data
                        AssetDatabase.CreateAsset(characterToCreate, $"Assets/Data/{tabs[tabIndex]}s/{characterToCreate.InGameName.ToLowerInvariant()}.asset");
                        AssetDatabase.SaveAssets();
                        AssetDatabase.Refresh();
                        GameDataList.Refresh();

                        characterToCreate = CreateInstance<Character>(); // Prepare next creation
                    }
                    break;
            }

        }

    }

}
